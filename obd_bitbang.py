"""
# File: obd_bitbang.py
# Author: Adam Fuller
# Date: 29 May 2018
# Description: OBD realtime receiver/parser.  Meant to run on
# Beaglebone Black.
"""

import time
import Adafruit_BBIO.GPIO as GPIO
import LEDs	
import threading
from multiprocessing import Queue
from collections import deque
import pickle
import math

OBD = 'P9_14'
dt = 0.008192 # bit duration [s].
n = 147 # message length in bits.
iLED=0

GPIO.cleanup()
GPIO.setup(OBD, GPIO.IN, GPIO.PUD_DOWN)



diesel_energy_density = 43.1e6 # [J/kg]
diesel_density = 832 # [kg/m3]
diesel_volumetric_energy_density = diesel_energy_density * diesel_density 
pprpc = 0.5 # fuel pulse per rev per cylinder
total_efficiency = 0.25
theta_injection_stroke = math.radians(30) # Full-stroke pump angle delta [pump radians]
N_cyl = 4

# Basic full-load injection volume (see 1KZ-TE manual for more
# detail).  Manual says it depends on the presence of BACS, pump
# speed, and boost.
# 
# Pump driven at half crank speed per Fuel System manual numbers.
# 4 shots / pump_rev.
#
# Diesel is 4 cycle per 2 rev, injection happens at 1 cycle only thats
# 1 shot/(2 engine_revs * cylinder) * 4 cylinder = 2 shots / engine_rev
# 
# pump_rev     2 shots      1 pump_rev
#---------- * --------    = ----------
#  4 shots     engine_rev   2 engine_rev
V_injection_pump = 14.7e-6/200

"""
OBD1 message format as an ordered list of (word_name,
ord_bit_length). word_bit_length includes the start and stop bits.
"""
_format = [
		('id', 4),
		('unknown1', 11),
		('injector pulse width', 11),
		('ignition timing angle', 11),
		('idle air control', 11),
		('engine speed', 11),
		('manifold absolute pressure', 11),
		('engine coolant temperature', 11),
		('throttle position sensor', 11),
		('speed [km/h]', 11),
		('unknown2', 11),
		('unknown3', 11),
		('flags1', 11),
		('flags2', 11),
		]

def _f_flags1(x):

	"""
	A function to parse the "Flags 1" word.
	"""

	flags = [
			'Cold start up',
			'Warm up',
			'Flags 1 Bit 2 (off idle?)',
			'Flags 1 Bit 3',
			'Flags 1 Bit 4',
			'Flags 1 Bit 5',
			'Flags 1 Bit 6',
			'Flags 1 Bit 7']

	bits = '{:08b}'.format(x)

	return dict(zip(flags, bits))

def _f_flags2(x):

	"""
	A function to parse the "Flags 2" word.
	"""

	bits = '{:08b}'.format(x)

	flags = [
			'Start (switch)',
			'Idle (throttle closed) switch)',
			'A/C switch (not?)',
			'Neutral switch',
			'Flags 2 Bit 4 (Park or Neutral)',
			'Flags 2 Bit 5 (AC clutch)',
			'Flags 2 Bit 6 (idling?)',
			'Diagnostics condition']

	return dict(zip(flags, bits))

"""
A dict of functions to apply to particular fields.
"""
_rules = {
	'engine speed': lambda x: x*25, # [rev/minute]
	'throttle position sensor': lambda x: x/2., # [deg]
	'injector pulse width': lambda x: x/10000., # [s]
	'ignition timing angle': lambda x: x-90., # [deg]
	'manifold absolute pressure': lambda x: x*1000., # [Pa]
	'flags1': _f_flags1,
	'flags2': _f_flags2,
	}

def _parse_word(s):

	"""
	Function to check start and stop bits and get 1 byte out.  Raises
	an exception if start or stop bits aren't right.
	"""

	#s = list(s.astype(int))

	# Check start bit
	assert s[0] == 0

	# Check stop bits
	assert s[9] == 1
	assert s[10] == 1

	# NB: Bit order is LSB.
	return eval('0b'+''.join(map(str, s[1:9][::-1])))

def post(data):

	"""
	Post-process a parsed OBD message and return to caller.
	"""

	data['engine speed [rad/s]'] = data['engine speed']/60.*2*math.pi
	data['engine speed [rev/s]'] = data['engine speed']/60.

	#data['pump speed [rad/s]'] = data['engine speed [rad/s]']*pprpc
	#try:
	#	data['full stroke duration [s]'] = theta_injection_stroke / data['pump speed [rad/s]']
	#except:
	#	data['full stroke duration [s]'] = 0

	#try:
	#	data['engine speed [s/rev]'] = 1./data['engine speed [rev/s]']
	#except:
	#	data['engine speed [s/rev]'] = 0

	data['injector pulse rate [1/s]'] = data['engine speed [rev/s]']*pprpc*N_cyl

	data['injector pulse width [pump deg]'] = data['injector pulse width']*data['engine speed [rad/s]']*180/math.pi*pprpc

	data ['injector pump volume [m3/pulse]'] = data['injector pulse width [pump deg]']/theta_injection_stroke*V_injection_pump 

	data['fuel rate [m3/s]'] = data ['injector pump volume [m3/pulse]']*data['injector pulse rate [1/s]']
	data['fuel rate [cc/s]'] = data['fuel rate [m3/s]']*1e6
	#data['fuel rate [l/h]'] = data['fuel rate [m3/s]']*3600/1000

	# XXX do road test to check speed units.
	#data['speed [m/s]'] = data ['speed [km/h]']*1000/3600

	#try:
	#	data['fuel rate [m3/m]'] = data['fuel rate [m3/s]'] / data['speed [m/s]']
	#except:
	#	data['fuel rate [m3/m]'] = 100
	#data['fuel rate [l/100km]'] = data['fuel rate [m3/m]'] * 1000 * 100000
	#data['fuel rate [gallon/mile]'] = data['fuel rate [m3/m]']*1000/3.78*1600
	#try:
	#	data['fuel rate [mile/gallon]'] = 1./data['fuel rate [gallon/mile]']
	#except:
	#	data['fuel rate [mile/gallon]'] = 0

	#data['burn rate [kW]'] = data['fuel rate [m3/s]'] * diesel_volumetric_energy_density / 1000
	#data['burn rate [hp]'] = data['burn rate [kW]'] * 1.34102

	#data['estimated power [kW]'] = data['burn rate [kW]'] * total_efficiency 

	return data

def parse(bits):
	"""
	list of ints : a sequence of 147 bits representing an OBD
	message.
	"""

	res = {}
	val = {}

	istart = 0

	# Loop over words
	for name, length in _format:

		if name:

			res[name] = bits[istart:istart+length]

			if name in _rules:
				x = _rules[name](_parse_word(res[name]))
			else:
				x = _parse_word(res[name])

			if type(x) == dict:
				val.update(x)
			else:
				val[name] = x
			
		istart += length
	
	return post(val)



def pp_dict(d):
	"""
	Pretty-print a dict.
	"""
	for k in sorted(d):
		print k, d[k]

def pps_dict(d):
	"""
	Pretty-print a dict to a string.
	"""
	return '\n'.join(['{} {}'.format(k,d[k]) for k in sorted(d)])

def get():

	"""
	This function samples the line.
	"""

	return GPIO.input(OBD)

def now():

	"""
	This function must return a time with precision suitable for use
	as a bit clock.
	"""

	return time.time()


def get_message(timeout):
	x = xold = 0
	in_msg = False
	tedge = now()
	while True:

		xold = x

		t = now()

		if (t-tedge)>timeout:

			print t, "timed out waiting for message"

			return None

		if not in_msg:
			# Sample promiscuously to detect message start.
			x = get()
		elif in_msg and t>=tsample:
			# It is time to sample a bit.
			if t>(tsample+0.5*dt):
				#print "timing error"
				return None
				#raise Exception('Timing error {} ms at bit {}.'.format((t-tsample)*1000, bit_position))
			x = get()
			#print '{:d}'.format(x)
			r.append(x)
			if len(r) == n:
				# Reached end of message.
				in_msg = False

				#print '<end>\n'

				return r

			else:
				tsample += dt
				#time.sleep(0.1*dt) # Consider increasing this sleep to free
				# up cycles in other threads.

		if x and not xold:
			# We are at a rising edge.
			if not in_msg:
				#print 'R'
				pass
			rising_edge = True
			falling_edge = False
			tedge = t
		elif not x and xold:
			# We are at a falling edge.
			if not in_msg:
				#print 'F'
				pass
			falling_edge = True
			rising_edge = False
			if t-tedge > 15*dt:
				# A falling edge following a long edge-free period indicates
				# the start of a message.
				#print "<Word start>"
				in_msg = True
				r = []
				tsample = t+dt/2 # Pick a good time to sample bit zero.
			tedge = t
		else:
			# We are not on an edge.
			rising_edge = falling_edge = False
			if t-tedge > 9*dt:
				# An edge-free period this long can't occur in a valid
				# message.
				if not in_msg:
					#print '-'
					pass
				else:
					#print "Word interrupted by long edge-free period.\n"
					pass
				in_msg = False
				r = []
	
		#if x:
		#	LEDs.on(iLED)
		#else:
		#	LEDs.off(iLED)
	
#def f_test(t=3):
#
#	import random
#	import time
#
#	time.sleep(t)
#
#	return random.random()

class async():

	"""
	Run an OBD listener in a new thread.  One may then check the .q
	Queue object at their leisure for tuples of (timestamp, message)
	as they become available.
	"""

	def __init__(self):

		#self.q = Queue(maxsize=10)
		self.q = deque(maxlen=10)
		self.killswitch = threading.Event()

		def f(q, killswitch):
			while not killswitch.is_set():
				m = get_message(timeout=2)
				if m:
					try:
						msg = parse(m)
					except:
						pass
					else:
						msg['time'] = now()
						pp_dict(msg)
						with open(fnview, 'w') as f:
							f.write(pps_dict(msg))
						q.append(msg)

		self.t = threading.Thread(target=f, args=(self.q, self.killswitch))
		self.t.daemon=True
		self.t.start()

	def kill(self):
		self.killswitch.set()
		self.t.join()

	def __del__(self):
		self.killswitch.set()
		self.t.join()

	def get(self):
		if self.q:
			return self.q.popleft()
		else:
			return None

if __name__ == '__main__':
	"""
	If you run this as a script it will start an OBD listener and
	wind up saving a pickled result when Ctrl-C'd.
	"""
	fnview = 'view.txt'
	log = []
	mt = now()
	a = async()
	nmax = 100000
	try:
		while len(log)<nmax:
			x = a.get()
			if x:
				t,msg = x
				#print '{:0.3f} s since last message.  '.format(t-mt), "Log has", len(log), "records."
				log.append(x)
				if msg:
					print
					print
					print
					pp_dict(msg)
					print t-mt
				mt = t
			else:
				pass
			time.sleep(0.1)
	except KeyboardInterrupt:
		ts = time.strftime('%Y%m%d_%Hh%Mm%Ss')
		with open('obd_log_{}.pkl'.format(ts), 'w') as f:
			pickle.dump(log, f)

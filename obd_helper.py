"""
# File: obd_helper.py
# Author: Adam Fuller
# Date: 22 June 2018
# Description: OBD post-processing.
"""
import pickle
import pandas as pd

def read_pkl(fn):
	return pd.DataFrame(pickle.load(open(fn))).set_index('time')

def read_old_pkl(fn):
	log = pickle.load(open(fn))
	r = []
	for a,b in log:
		if b:
			b['time'] = a
			r.append(b)
	return pd.DataFrame(r).set_index('time')
